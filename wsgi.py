from flask import Flask
from flask import request
from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp
import datetime
import requests


class User(object):
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id

user_resp = requests.get('http://hitronet.pp.ua/api/auth.php')
users = [
    User(user_resp.json()['id'],user_resp.json()['username'],user_resp.json()['password']),
]

username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}


def authenticate(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user


def identity(payload):
    user_id = payload['identity']
    return userid_table.get(user_id, None)


class BotHandler:

    def __init__(self, token):
        self.token = token
        self.api_url = "https://api.telegram.org/bot{}/".format(token)

    def check(self, offset=None, timeout=1):
        method = 'getUpdates'
        params = {'timeout': timeout, 'offset': offset}
        resp = requests.get(self.api_url + method, params)
        result_json = resp.json()['ok']
        if result_json == 'false':
            return False
        else:
            return True

    def send_message(self, chat_id, text):
        params = {'chat_id': chat_id, 'text': text}
        method = 'sendMessage'
        resp = requests.post(self.api_url + method, params)
        return resp


application = Flask(__name__)
application.debug = True
application.config['SECRET_KEY'] = 'Zhakard-sinchuk'

jwt = JWT(application, authenticate, identity)


@application.route('/api/send', methods=["POST"])
@jwt_required()
def post():
    content = request.get_json()
    number = content['phone']
    token = content['token']
    if 4 < len(number) < 14:
        greet_bot = BotHandler(token)
        now = datetime.datetime.now()

        def main():

            today = now.day
            hour = now.hour

            checkbot = greet_bot.check()
            if checkbot:

                last_chat_id = '-1001265618908'
                last_chat_name = 'Zhakard'

                if today == now.day and 6 <= hour < 12:
                    greet_bot.send_message(last_chat_id, 'Добрий ранок, {}, номер: {}'.format(last_chat_name, number))
                    today += 1

                elif today == now.day and 12 <= hour < 17:
                    greet_bot.send_message(last_chat_id, 'Добрый день, {}, номер: {}'.format(last_chat_name, number))
                    today += 1

                elif today == now.day and 17 <= hour < 23:
                    greet_bot.send_message(last_chat_id, 'Добрый вечір, {}, номер: {}'.format(last_chat_name, number))
                    today += 1
                
                else:
                   greet_bot.send_message(last_chat_id, 'Доброї ночі, {}, номер: {}'.format(last_chat_name, number))
                   today += 1

        main()

        return '', 200
    else:
        return '', 400


if __name__ == "__main__":
    application.run()
